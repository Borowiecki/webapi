﻿using System;
using System.Collections.Generic;
using WebApi.RestRoutes;
using WebApi.Shared;
using WebApi.Shared.Models;
using WebApi.Shared.RestHttpClient;

namespace WebApi.Client
{
    public class Program
    {
        private static readonly WebApiRestClient _client = new WebApiRestClient();
        private static readonly string _baseUri = @"https://localhost:44376";

        public static void Main(string[] args)
        {
            StartClient();
        }

        private static void StartClient()
        {
            GetCites();
            Console.WriteLine();
            CreateCity();
            Console.WriteLine();
            GetCites();
            Console.ReadLine();
        }

        public static void GetCites()
        {
            var response = _client.Post<List<City>>($"{CityControllerRoutes.Controller}/{CityControllerRoutes.GetAll}");
            foreach(var city in response)
            {
                Console.WriteLine(city.Name);
            }
        }

        public static void CreateCity()
        {
            var response = _client.Post<Response, City>(new City { CityId = 3, Name = "gowno" }, $"{CityControllerRoutes.Controller}/{CityControllerRoutes.Create}");
            Console.WriteLine(response.Description);
        }

        private static void GetPeople()
        {
            var response = _client.Post<List<Person>>($"{PersonController.Controller}/{PersonController.GetAll}");
            foreach (var person in response)
            {
                Console.WriteLine(person.Name);
            }
        }

        private static void GetPerson()
        {
            var response = _client.Post<Person, Wrapper<int>>(new Wrapper<int>(1), $"{PersonController.Controller}/{PersonController.Get}");

            Console.WriteLine(response.Name);
        }

        private static void CreatePerson()
        {
            var response = _client.Post<Response, Person>(new Person { PersonId = 3, Name = "Piotrek", Age = 5 }, $"{PersonController.Controller}/{PersonController.Create}");

            Console.WriteLine(response.Description);
        }

        private static void DeletePerson()
        {
            var response = _client.Post<Response, Wrapper<int>>(new Wrapper<int>(3), $"{PersonController.Controller}/{PersonController.Delete}");
            Console.WriteLine(response.Description);
        }


        private static void UpdatePerson()
        {
            var response = _client.Post<Response, Person>(new Person { PersonId = 1, Name = "Hehszki" }, $"{PersonController.Controller}/{PersonController.Update}");
            Console.WriteLine(response.Description);
        }
    }
}
