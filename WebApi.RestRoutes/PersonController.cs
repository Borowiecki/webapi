﻿namespace WebApi.RestRoutes
{
    public class PersonController
    {
        
        public const string Controller = "api/values";
        public const string Get = "get";
        public const string GetAll = "getAll";
        public const string Create = "create";
        public const string Update = "update";
        public const string Delete = "delete";
    }
}
