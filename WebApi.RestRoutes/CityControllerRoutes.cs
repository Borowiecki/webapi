﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebApi.RestRoutes
{
    public class CityControllerRoutes
    {
        public const string Controller = "api/city";
        public const string Get = "get";
        public const string GetAll = "getAll";
        public const string Create = "create";
        public const string Update = "update";
        public const string Delete = "delete";
    }
}
