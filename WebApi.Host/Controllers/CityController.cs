﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebApi.OperationProvider;
using WebApi.RestRoutes;
using WebApi.Shared;
using WebApi.Shared.Models;

namespace WebApi.Host.Controllers
{
    [Route(CityControllerRoutes.Controller)]
    [ApiController]
    public class CityController : ControllerBase
    {
        private CityOperationProvider _provider = new CityOperationProvider();

        [HttpPost(CityControllerRoutes.GetAll)]
        public ActionResult<RestResponse<List<City>>> GetAll()
        {
            return Ok(
                new RestResponse<List<City>>(_provider.GetAll())
                );
        }

        [HttpPost(CityControllerRoutes.Get)]
        public ActionResult<RestResponse<City>> Get(Wrapper<int> id)
        {
            return Ok(
                new RestResponse<City>(_provider.GetCity(id.Value))
                );
        }

        [HttpPost(CityControllerRoutes.Create)]
        public ActionResult<RestResponse<Response>> Create(City city)
        {
            return Ok(
                new RestResponse<Response>(_provider.Create(city))
                );
        }

        [HttpPost(CityControllerRoutes.Update)]
        public ActionResult<RestResponse<Response>> Update(City city)
        {
            return Ok(
                new RestResponse<Response>(_provider.Update(city))
                );
        }

        [HttpPost(CityControllerRoutes.Delete)]
        public ActionResult<RestResponse<Response>> Delete(Wrapper<int> id)
        {
            return Ok(
                new RestResponse<Response>(_provider.Delete(id.Value))
                );
        }
    }
}