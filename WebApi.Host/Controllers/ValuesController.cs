﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using WebApi.OperationProvider;
using WebApi.RestRoutes;
using WebApi.Shared;
using WebApi.Shared.Models;

namespace WebApi.Host.Controllers
{
    [Route(PersonController.Controller)]
    [ApiController]
    public class ValuesController : ControllerBase
    {
        private PersonOperationsProvider _personProvider = new PersonOperationsProvider();

        [HttpPost(PersonController.GetAll)]
        public ActionResult<RestResponse<Person>> Get()
        {
            return Ok(
                new RestResponse<List<Person>>(
                    _personProvider.GetAll()));
        }

        [HttpPost(PersonController.Get)]
        public ActionResult<RestResponse<Person>> Get(Wrapper<int> id)
        {
            return Ok(
                new RestResponse<Person>(
                    _personProvider.Get(id.Value)));
        }

        [HttpPost(PersonController.Create)]
        public ActionResult<Response> Post(Person person)
        {
            return Ok(
                new RestResponse<Response>(
                    _personProvider.Create(person)));
        }

        [HttpPost(PersonController.Update)]
        public ActionResult<Response> Put(Person person)
        {
            return Ok(
                new RestResponse<Response>(
                    _personProvider.Update(person)));
        }

        [HttpPost(PersonController.Delete)]
        public ActionResult<Response> Delete(Wrapper<int> id)
        {
            return Ok(
                new RestResponse<Response>(
                    _personProvider.Delete(id.Value)));
        }
    }
}
