﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using WebApi.Shared.Models;

namespace WebApi.Shared.RestHttpClient
{
    public class WebApiRestClient
    {
        private static readonly HttpClient _client = new HttpClient();
        private static readonly string _baseUri = @"https://localhost:44376";

        public ReturnType Post<ReturnType>(string uri)
        {
            var methodUri = new Uri($"{_baseUri}/{uri}");
            var httpRequest = new HttpRequestMessage(HttpMethod.Post, methodUri)
            {
                Content = null
            };

            var response = _client.SendAsync(httpRequest).Result;
            var body = response.Content.ReadAsStringAsync().Result;
            var deserialized = JsonConvert.DeserializeObject<RestResponse<ReturnType>>(body);

            return deserialized.Value;
        }

        public ReturnType Post<ReturnType, ArgumenType>(ArgumenType argument, string uri)
        {
            var methodUri = new Uri($"{_baseUri}/{uri}");
            var json = JsonConvert.SerializeObject(argument);

            var content = new StringContent(json, Encoding.UTF8, "application/json");
            var httpRequest = new HttpRequestMessage(HttpMethod.Post, methodUri)
            {
                Content = content
            };
            var response = _client.SendAsync(httpRequest).Result;
            var body = response.Content.ReadAsStringAsync().Result;
            var deserialized = JsonConvert.DeserializeObject<RestResponse<ReturnType>>(body);

            return deserialized.Value;
        }
    }
}
