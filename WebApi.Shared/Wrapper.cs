﻿namespace WebApi.Shared
{
    public class Wrapper<T>
    {
        public T Value { get; set; }
        public Wrapper(T value)
        {
            Value = value;
        }
        public Wrapper()
        {

        }
    }
}
