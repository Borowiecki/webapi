﻿namespace WebApi.Shared.Models
{
    public class RestResponse<T>
    {
        public T Value { get; set; }

        public RestResponse(T value)
        {
            Value = value;
        }

    }

    public class RestResponse
    {

    }
}
