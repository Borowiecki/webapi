﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebApi.Shared.Models
{
    public class Response
    {
        public bool Result { get; set; }
        public string Description { get; set; }
        public int? NewId { get; set; }

        public Response()
        {
            Result = true;
            Description = "Success";
            NewId = null;
        }
    }
}
