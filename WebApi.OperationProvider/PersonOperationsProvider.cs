﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WebApi.Shared.Models;

namespace WebApi.OperationProvider
{
    public class PersonOperationsProvider
    {
        private static List<Person> _list = new List<Person>
        {
            new Person
            {
                Age = 12,
                Name = "Tomek",
                PersonId = 1
            },
            new Person
            {
                Age = 18,
                Name = "Asia",
                PersonId = 2
            }
        };

        public List<Person> GetAll()
        {
            return _list;
        }

        public Person Get(int id)
        {
            return _list.Where(x => x.PersonId == id).FirstOrDefault();
        }

        public Response Update(Person person)
        {
            var listPerson = _list.Where(x => x.PersonId == person.PersonId).FirstOrDefault();
            listPerson.Name = person.Name;

            return new Response();
        }

        public Response Create(Person person)
        {
            _list.Add(person);

            return new Response();
        }

        public Response Delete(int id)
        {
            _list.Remove(_list.Where(x => x.PersonId == id).FirstOrDefault());

            return new Response();
        }
    }
}
