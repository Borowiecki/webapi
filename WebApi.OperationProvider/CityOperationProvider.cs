﻿using System.Collections.Generic;
using System.Linq;
using WebApi.Shared.Models;

namespace WebApi.OperationProvider
{
    public class CityOperationProvider
    {
        private static readonly List<City> _list = new List<City>
        {
            new City
            {

                Name = "Tomek",
                CityId = 1
            },
            new City
            {
                Name = "Asia",
                CityId = 2
            }
        };

        public List<City> GetAll()
        {
            return _list;
        }

        public City GetCity(int id)
        {
            return _list.Where(x => x.CityId == id).FirstOrDefault();
        }

        public Response Create(City city)
        {
            _list.Add(city);
            return new Response();
        }

        public Response Update(City city)
        {
           var tmp = _list.Where(x => x.CityId == city.CityId).FirstOrDefault();
           tmp.Name = city.Name;

            return new Response();
        }

        public Response Delete(int id)
        {
            _list.Remove(_list.Where(x => x.CityId == id).FirstOrDefault());

            return new Response();
        }
    }
}
